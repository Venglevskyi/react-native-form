import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    paddingLeft: 8,
    paddingRight: 8,
  },
  surface: {
    padding: 8,
    elevation: 6,
  },
  btnReg: {
    marginTop: 20,
  },
  errorMsg: {
    color: "red",
    paddingLeft: 8,
  },
});

export { styles };
