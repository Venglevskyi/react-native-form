const rules = {
  required: true,
  minLength: { value: 4, message: "To short!" },
  maxLength: { value: 10, message: "To long!" },
};

const promisification = () => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      const randomInt = Math.random() > 0.3;

      if (randomInt) {
        resolve();
      } else {
        reject();
      }
    }, 1000);
  });
};

export { rules, promisification };
