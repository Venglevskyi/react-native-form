import React, { useState } from "react";
import { useDispatch } from "react-redux";
import {
  TextInput,
  Surface,
  Modal,
  Portal,
  Text,
  Button,
} from "react-native-paper";
import { View } from "react-native";
import { useForm, Controller } from "react-hook-form";

import { styles } from "./style";
import { authUser } from "../../redux/authActions";
import { rules, promisification } from "./options";

const FormScreenHooks = ({ navigation, route }) => {
  const { values } = route.params;

  const dispatch = useDispatch();
  const { control, errors, handleSubmit, formState } = useForm({
    mode: "onChange",
  });

  const [visible, setVisible] = useState(false);
  const [loading, setLoading] = useState(false);
  const [login, setLogin] = useState("");
  const [gender, setGender] = useState("");

  const showModal = () => setVisible(true);
  const hideModal = () => setVisible(false);
  const containerStyle = {
    backgroundColor: "white",
    padding: 10,
    height: 300,
    marginLeft: 10,
    marginRight: 10,
  };

  const onSubmit = () => {
    setLoading(true);

    const fullInfoUser = { ...values, login, gender };
    dispatch(authUser(fullInfoUser));

    promisification()
      .then(() => navigation.navigate("SuccessScreen"))
      .catch(() => showModal())
      .finally(() => setLoading(false));
  };

  return (
    <View style={styles.container}>
      {visible && (
        <Portal>
          <Modal
            visible={visible}
            onDismiss={hideModal}
            contentContainerStyle={containerStyle}
          >
            <Text style={{ textAlign: "center", color: "black" }}>
              Something went wrong...
            </Text>
            <Button onPress={hideModal}>Close</Button>
          </Modal>
        </Portal>
      )}
      <View style={styles.container}>
        <Surface style={styles.surface}>
          <Controller
            render={({ onChange, onBlur, value }) => (
              <TextInput
                mode="outlined"
                label="Login"
                onBlur={onBlur}
                onChangeText={(value) => {
                  onChange(value);
                  setLogin(value);
                }}
                value={value}
              />
            )}
            name="login"
            rules={rules}
            control={control}
            defaultValue=""
          />
          {errors.login && (
            <Text style={styles.errorMsg}>{errors.login?.message}</Text>
          )}
          <Controller
            render={({ onChange, onBlur, value }) => (
              <TextInput
                mode="outlined"
                label="Gender"
                onBlur={onBlur}
                onChangeText={(value) => {
                  onChange(value);
                  setGender(value);
                }}
                value={value}
              />
            )}
            name="gender"
            rules={rules}
            control={control}
            defaultValue=""
          />
          {errors.gender && (
            <Text style={styles.errorMsg}>{errors.gender?.message}</Text>
          )}
          <Button
            mode="contained"
            loading={loading}
            onPress={handleSubmit(onSubmit)}
            disabled={!formState.isDirty || !formState.isValid}
            style={styles.btnReg}
          >
            Register
          </Button>
        </Surface>
      </View>
    </View>
  );
};
export default FormScreenHooks;
