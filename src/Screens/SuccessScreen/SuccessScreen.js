import React from "react";
import { useSelector } from "react-redux";
import { View, Text } from "react-native";
import { Title } from "react-native-paper";

import { styles } from "./style";

const SuccessScreen = () => {
  const userInfo = useSelector((state) => state.auth);
  return (
    <View style={styles.container}>
      <Title style={styles.text}>
        Сongratulations you registered successfully!!!
      </Title>
      <Text style={styles.text}>{userInfo.name}</Text>
      <Text style={styles.text}>{userInfo.email}</Text>
      <Text style={styles.text}>{userInfo.password}</Text>
      <Text style={styles.text}>{userInfo.login}</Text>
      <Text style={styles.text}>{userInfo.gender}</Text>
    </View>
  );
};

export default SuccessScreen;
