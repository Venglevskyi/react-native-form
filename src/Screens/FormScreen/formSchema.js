import * as yup from "yup";

const formSchema = yup.object().shape({
  name: yup.string().min(2, "Too Short!").max(30, "Too Long!").required(),
  email: yup.string().email("Invalid email").required(),
  password: yup
    .string()
    .min(6, "Minimum 6 characters required")
    .matches(/(?=.*[a-za-я])/, "at least 1 lowercase character required")
    .matches(/(?=.*[A-ZА-Я])/, "at least 1 uppercase character required")
    .matches(/(?=.*[!@#$%^&*])/, "at least one special required")
    .required(),
});

export { formSchema };
