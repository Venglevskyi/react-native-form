import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    paddingLeft: 8,
    paddingRight: 8,
  },
  surface: {
    padding: 16,
    elevation: 3,
  },
  errorMsg: {
    color: "red",
    paddingLeft: 8,
  },
  btnNext: {
    marginTop: 8,
  },
  textOr: {
    textAlign: "center",
    marginTop: 20,
    marginBottom: 20,
    fontSize: 18,
  },
});

export { styles };
