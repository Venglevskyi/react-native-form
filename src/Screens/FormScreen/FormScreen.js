import React, { useState, useEffect } from "react";
import { TextInput, Button, Surface, Checkbox, Text } from "react-native-paper";
import { useFormik } from "formik";
import { View, ScrollView } from "react-native";
import { LoginButton, AccessToken, LoginManager } from "react-native-fbsdk";
import {
  GoogleSignin,
  GoogleSigninButton,
  statusCodes,
} from "@react-native-community/google-signin";
import SplashScreen from "react-native-splash-screen";

import { formSchema } from "./formSchema";
import { styles } from "./style";

const FormScreen = ({ navigation }) => {
  const [checked, setChecked] = useState(false);

  useEffect(() => {
    SplashScreen.hide();
    GoogleSignin.configure();
    // GoogleSignin.configure({
    //   webClientId:
    //     "1061813062599-picvkn6ldvploipjqn7v39egt8stg6r2.apps.googleusercontent.com",
    //   offlineAccess: true,
    // });
  }, []);

  const formik = useFormik({
    initialValues: {
      name: "",
      email: "",
      password: "",
    },
    onSubmit: (values, actions) => {
      // actions.resetForm();
      navigation?.navigate("FormScreenHooks", { values });
    },
    validationSchema: formSchema,
  });

  const {
    values,
    touched,
    errors,
    dirty,
    isValid,
    handleChange,
    handleBlur,
    handleSubmit,
  } = formik;

  const loginWithFacebook = () => {
    LoginManager.logInWithPermissions(["public_profile", "email"]).then(
      function (result) {
        if (result.isCancelled) {
          console.log("==> Login cancelled");
        } else {
          AccessToken.getCurrentAccessToken().then((data) => {
            alert(data.accessToken.toString());
          });
        }
      },
      function (error) {
        console.log("==> Login fail with error: " + error);
      }
    );
  };

  const _signIn = async () => {
    try {
      await GoogleSignin.hasPlayServices();
      const userInfo = await GoogleSignin.signIn();
      const { user } = userInfo;
      alert(`user info: name: ${user.name}, email: ${user.email}`);
    } catch (error) {
      if (error.code === statusCodes.SIGN_IN_CANCELLED) {
        console.log("SIGN_IN_CANCELLED");
      } else if (error.code === statusCodes.IN_PROGRESS) {
        console.log("IN_PROGRESS");
      } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
        console.log("PLAY_SERVICES_NOT_AVAILABLE");
      } else {
        console.log(error);
      }
    }
  };

  return (
    <View style={styles.container}>
      <Surface style={styles.surface}>
        <ScrollView>
          <TextInput
            mode="outlined"
            label="Name"
            autoCapitalize="none"
            value={values.name}
            onChangeText={handleChange("name")}
            onBlur={handleBlur("name")}
          />
          {touched.name && <Text style={styles.errorMsg}>{errors.name}</Text>}
          <TextInput
            mode="outlined"
            label="Email"
            autoCapitalize="none"
            value={values.email}
            onChangeText={handleChange("email")}
            onBlur={handleBlur("email")}
          />
          {touched.email && <Text style={styles.errorMsg}>{errors.email}</Text>}
          <TextInput
            secureTextEntry={!checked}
            mode="outlined"
            label="Password"
            autoCapitalize="none"
            value={values.password}
            onChangeText={handleChange("password")}
            onBlur={handleBlur("password")}
          />
          {touched.password && (
            <Text style={styles.errorMsg}>{errors.password}</Text>
          )}
          <View style={{ flexDirection: "row" }}>
            <Checkbox
              status={checked ? "checked" : "unchecked"}
              color="#397AF9"
              onPress={() => {
                setChecked(!checked);
              }}
            />
            <Text style={{ marginTop: 8 }}>Show password</Text>
          </View>
          <Button
            icon="arrow-right-circle-outline"
            mode="contained"
            disabled={!dirty || !isValid}
            onPress={handleSubmit}
            style={styles.btnNext}
          >
            Next
          </Button>
          <Text style={styles.textOr}>or</Text>
          <Button
            icon="facebook"
            onPress={loginWithFacebook}
            mode="contained"
            style={{ marginBottom: 30 }}
          >
            <Text>Login with Facebook</Text>
          </Button>
          <Button onPress={_signIn} mode="contained" icon="google">
            <Text>Login with Google</Text>
          </Button>
        </ScrollView>
      </Surface>
    </View>
  );
};
export default FormScreen;
