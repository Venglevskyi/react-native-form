import React, { useState } from "react";
import { Switch, useTheme } from "react-native-paper";

const Toogle = ({ onToggleTheme }) => {
  const [isSwitchOn, setIsSwitchOn] = useState(false);
  const paperTheme = useTheme();

  const onToggleSwitch = () => {
    setIsSwitchOn(!isSwitchOn);
    onToggleTheme();
  };

  return (
    <Switch
      value={paperTheme.dark}
      onValueChange={onToggleSwitch}
      color="#397AF9"
    />
  );
};

export default Toogle;
