import React, { useState } from "react";
import {
  NavigationContainer,
  DefaultTheme as NavigationDefaultTheme,
  DarkTheme as NavigationDarkTheme,
} from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import "react-native-gesture-handler";
import {
  DarkTheme as PaperDarkTheme,
  DefaultTheme as PaperDefaultTheme,
  Provider as PaperProvider,
} from "react-native-paper";
import merge from "deepmerge";

import Header from "../Header/Header";
import FormScreen from "../../Screens/FormScreen";
import FormScreenHooks from "../../Screens/FormScreenHooks";
import SuccessScreen from "../../Screens/SuccessScreen";

const Stack = createStackNavigator();
const CombinedDefaultTheme = merge(PaperDefaultTheme, NavigationDefaultTheme);
const CombinedDarkTheme = merge(PaperDarkTheme, NavigationDarkTheme);

const StackNavigation = () => {
  const [isDarkTheme, setIsDarkTheme] = useState(false);

  const theme = isDarkTheme ? CombinedDarkTheme : CombinedDefaultTheme;

  const toggleTheme = () => {
    setIsDarkTheme(!isDarkTheme);
  };

  return (
    <PaperProvider theme={theme}>
      <NavigationContainer theme={theme}>
        <Stack.Navigator
          initialRouteName="FormScreen"
          headerMode="screen"
          screenOptions={{
            header: ({ scene, previous, navigation }) => (
              <Header
                scene={scene}
                previous={previous}
                navigation={navigation}
                toggleTheme={toggleTheme}
              />
            ),
          }}
        >
          <Stack.Screen
            name="FormScreen"
            component={FormScreen}
            options={{ headerTitle: "Register", subTitle: "step 1" }}
          />
          <Stack.Screen
            name="FormScreenHooks"
            component={FormScreenHooks}
            options={{ headerTitle: "Register", subTitle: "step 2" }}
          />
          <Stack.Screen
            name="SuccessScreen"
            component={SuccessScreen}
            options={{ headerTitle: "Register", subTitle: "success" }}
          />
        </Stack.Navigator>
      </NavigationContainer>
    </PaperProvider>
  );
};

export default StackNavigation;
