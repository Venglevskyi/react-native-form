import React from "react";
import { store } from "./src/redux/store";
import { Provider } from "react-redux";

import StackNavigation from "./src/Components/StackNavigation";

const App = () => (
  <Provider store={store}>
    <StackNavigation />
  </Provider>
);

export default App;
